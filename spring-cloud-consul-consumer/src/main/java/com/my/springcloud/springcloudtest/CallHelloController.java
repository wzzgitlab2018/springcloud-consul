package com.my.springcloud.springcloudtest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CallHelloController {

    @Autowired
    private LoadBalancerClient loadBalancer;

    @RequestMapping("/call")
    public String call() {
        ServiceInstance serviceInstance = loadBalancer.choose("service-producer");
        System.out.println("服务地址：" + serviceInstance.getUri());
        System.out.println("服务名称：" + serviceInstance.getServiceId());
        Map<String, String> param = new HashMap<String, String>();
        param.put("name", "consul");
        String callServiceResult = null;
        try{
	        callServiceResult = new RestTemplate().getForObject(serviceInstance.getUri().toString() + "/hello?name=consul", String.class, param);
	        System.out.println(callServiceResult);
        }catch(Exception e){
        	e.printStackTrace();
        }
        return callServiceResult;
    }

}
